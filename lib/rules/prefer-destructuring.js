const ruleComposer = require('eslint-rule-composer');
const { Linter } = require('eslint');

const preferDestructuring = new Linter().getRules().get('prefer-destructuring');

module.exports = ruleComposer.filterReports(
  preferDestructuring,
  (problem, _metadata) => {
    if (problem.messageId === 'preferDestructuring' && problem.node.type === 'VariableDeclarator') {
      const { id, init } = problem.node;
      if (id.type === 'Identifier' && init.type === 'MemberExpression') return true;
    }
    return false;
  }
);
