# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 1.0.1 (2020-11-06)


### Bug Fixes

* **DestructureDepth:** Fixed error that occurred when a user would add default text to variable declaration ([c7516c6](https://cf_git///commit/c7516c687b308bda4a172ffa047c9cc07ef43c01))
* Finished custom prefer-destructuring rule ([5c7464f](https://cf_git///commit/5c7464fd63c3e2ec0a90520fc3a6a0d6f301a855)), closes [#174](https://app.clubhouse.io/core-fabrications/story/174)


* update release script ([55a3331](https://cf_git///commit/55a3331ed877ba3aee371e9a6ad67808271fc179))
* **release:** 1.0.0 ([8d0632b](https://cf_git///commit/8d0632bbc6699235e2c8dff0a0e1d1a98d7dd867))
* Initial Commit ([60b1e4b](https://cf_git///commit/60b1e4bfafe3da2b86ff43234a8e4ec6f57f040d))

## 1.0.0 (2020-11-06)


### Bug Fixes

* **DestructureDepth:** Fixed error that occurred when a user would add default text to variable declaration ([c7516c6](https://cf_git///commit/c7516c687b308bda4a172ffa047c9cc07ef43c01))
* Finished custom prefer-destructuring rule ([5c7464f](https://cf_git///commit/5c7464fd63c3e2ec0a90520fc3a6a0d6f301a855)), closes [#174](https://app.clubhouse.io/core-fabrications/story/174)


* Initial Commit ([60b1e4b](https://cf_git///commit/60b1e4bfafe3da2b86ff43234a8e4ec6f57f040d))

## 1.0.0 (2020-11-05)
