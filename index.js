module.exports = {
  rules: {
    'max-static-destructure-depth': require('./lib/rules/max-static-destructure-depth'),
    'prefer-destructuring': require('./lib/rules/prefer-destructuring')
  },
  configs: {
    recommended: {
      plugins: [
        '@core-fabrications/eslint-plugin'
      ],
      extends: 'eslint:recommended',
      env: {
        es6: true,
        node: true,
      },
      parserOptions: {
        ecmaVersion: 2018
      },
      rules: {
        semi: ['error', 'always'],
        quotes: ['error', 'single', { avoidEscape: true, allowTemplateLiterals: true }],
        curly: ['error', 'multi-or-nest'],
        'arrow-body-style': ['error', 'as-needed'],
        'arrow-parens': ['error', 'as-needed'],
        '@core-fabrications/max-static-destructure-depth': ['error', 2],
        '@core-fabrications/prefer-destructuring': ['error', { array: true, object: true }, { enforceForRenamedProperties: true }]
      }
    }
  }
}