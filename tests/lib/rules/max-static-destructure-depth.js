/**
 * @fileoverview Fuck you
 * @author Core Fabrications
 */
"use strict";
// const test = require('ava');
//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require("../../../lib/rules/max-static-destructure-depth"),
    RuleTester = require("eslint").RuleTester;

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester({
    env: {
        es6: true
    }
});

ruleTester.run("max-static-destructure-depth", rule, {
    valid: [
        // Check destructure with default values
        {
            code: 'const { DB_NAME = \'dev\', DB_HOST = \'127.0.0.1\', DB_PASS = \'verySecure\' } = process.env'
        },
        // check destructure without default values
        {
            code: "const {a,b,c} = some.variable.depth",
            options: [5]
        }
    ],
    invalid: [
        {
            code: "const {a, b, c} = process.env.something",
            errors: [{
                messageId: 'maximumDepthBreach',
                data: {
                    actual: 3,
                    allowed: 2
                }
            }]
        },
        {
            code: 'const {a,b,c} = some.variable.depth.longer',
            errors: [{
                messageId: 'maximumDepthBreach',
                data: {
                    actual: 4,
                    allowed: 3
                }
            }],
            options: [3]
        }
    ],

});
