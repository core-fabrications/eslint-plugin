/**
 * 
 * @fileoverview Fuck you
 * @author Core Fabrications
 */
"use strict";
// const test = require('ava');
//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require("../../../lib/rules/prefer-destructuring"),
  RuleTester = require("eslint").RuleTester;

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  env: {
    es6: true
  }
});

ruleTester.run("prefer-destructuring", rule, {
  valid: [
    {
      code: "var [ foo ] = array;",
    },
    {
      code: "var foo = array[someIndex];",
    },
    {
      code: "var { bar } = object.foo"
    }
  ],
  invalid: [
    {
      code: "var foo = array[0];",
      errors: [{
        messageId: 'preferDestructuring'
      }]
    },
    {
      code: 'const foo = object[\'foo\']',
      errors: [{
        messageId: 'preferDestructuring',
      }]
    }
  ]
});
